package com.template.controller;

import com.template.model.User;
import com.template.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/*
The @Controller is simply a specialization of the @Component class and allows implementation
classes to be autodetected throught the classpath scanning.
@RestController is a specialized version of @Controller.The @RestController is a combination of
@Controller and @ResponseBody annotations.It simplifies the controller implementation.

The job of @Controller is to create a Map of the model object and find a view but @RestController simply
returns the object and object data is directly written into HTTP response as JSON or XML.
 */
@RestController
public class UserController {

    private UserService userService;

    // Constructor based dependency-injection.No need for @Autowired on the constructor.
    // See other types of dependency injection: setter based DI and field based DI.
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /*
    @GetMapping maps /user to the getUser method. When starting the spring boot project,
    the app default hostname and port will be localhost and 8080.When the url is invoked in a web browser
    or using curl on the command line, the method returns a JSON,the default data format.You can also change
    it to XML.To access it in a browser or in a REST API Client like Postman or Insomnia, put url
    http://localhost:8080/user to access the method and return the JSON object.
    To access it in a command line, the easiest way is to use your IDEA terminal, put in there
    the commnad curl localhost:8080/user and you should see the JSON.
     */
    @GetMapping("/user")
    public User getUser() {
        return userService.getUser();
    }
}
