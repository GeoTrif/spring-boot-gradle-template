package com.template.service;

import com.template.model.User;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    public User getUser() {
        User user = new User();
        user.setName("Geo Trif");
        user.setAge(28);
        user.setAddress("St. New York No.4");
        user.setAddress("geo.trif@email.com");
        user.setIdNumber(123456789L);

        return user;
    }
}
