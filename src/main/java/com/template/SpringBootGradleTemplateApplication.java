package com.template;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

/*
@SpringBootApplication is a convenient annotation that adds:
 - @Configuration : Tags the class as a source of bean definitions for the application context.
 - @EnableAutoConfiguration : Tells Spring Boot to start adding beans based on classpath settings,
                              .other beans and various property settings.For example, if spring-webmvc
                              is on the classpath, this annotation flags the application as a web application
                              and activates key behaviours, such as setting up a DispatcherServlet.
 - @ComponentScan : Tells Spring to look for other components,configurations and services in the base
                    package, letting it find the controllers.
 */
@SpringBootApplication
public class SpringBootGradleTemplateApplication {

    // The main method uses Spring Boot's SpringApplication.run method to launch an application.
    public static void main(String[] args) {
        SpringApplication.run(SpringBootGradleTemplateApplication.class, args);
    }

    /*
    @Bean indicates that the method produces a bean to be managed by the Spring container.
    Beans are objects that are instantiated,assembled and managed by the Spring IoC Container.
    The commandLineRunner method runs on startup.It retrieves all the beans that were created
    by the application or that were automatically added by Spring Boot.
     */
    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext context) {
        return args -> {
            System.out.println("Beans provided by Spring Boot:");
            String[] beanNames = context.getBeanDefinitionNames();
            Arrays.sort(beanNames);

            for (String beanName : beanNames) {
                System.out.println(beanName);
            }
        };
    }
}
