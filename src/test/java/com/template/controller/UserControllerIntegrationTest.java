package com.template.controller;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

import java.net.URL;

/**
 * Full-stack integratio test.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerIntegrationTest {

    private static final String ACTUAL_JSON = "{\"name\":\"Geo Trif\",\"age\":28,\"address\":\"geo.trif@email.com\",\"email\":null,\"idNumber\":123456789}";
    private static final String PROTOCOL_AND_HOSTNAME = "http://localhost:";
    private static final String USER_PATH = "/user";

    @LocalServerPort
    private int port;

    private URL baseUrl;

    @Autowired
    private TestRestTemplate template;

    @BeforeEach
    public void setUp() throws Exception {
        this.baseUrl = new URL(PROTOCOL_AND_HOSTNAME + port + USER_PATH);
    }

    @Test
    public void getUser() {
        ResponseEntity<String> response = template.getForEntity(baseUrl.toString(), String.class);

        assertThat(response.getBody()).isEqualTo(ACTUAL_JSON);
    }
}
